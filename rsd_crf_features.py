# -*- coding: utf-8 -*-

# CRF features for RoSyllabiDict dataset

import os.path

from lxml import etree
import unicodedata

from sklearn.model_selection import train_test_split
import sklearn

from constants import ENCODING

print(sklearn.__version__)
# 0.23.2

print(unicodedata.unidata_version)


# 11.0.0


def u8n(w, form='NFKD'):
    return unicodedata.normalize(form, w)


# constant for the accent character
ACCENT = list({c for c in u8n(u'áéíóú') if c not in 'aeiou'})[0]
DIACRITICS = {c for c in u8n('ăîâţț') if c not in 'ait'}


def read_data(max_examples=0):
    x = 0
    selected_words = []
    all_initial_words = []
    # word_dict = {}
    with open('datasets/silabe-finale_fix.xml', mode='r', encoding=ENCODING) as f:
        for i, line in enumerate(f):
            if line[0:5] != "<form":
                continue
            xml = etree.XML(line)
            # discard leading whitespace
            ws = xml.text[1:]
            new_word = {'w': xml.attrib['w'], 'text': ws, 'x': ''.join(c for c in u8n(ws) if c != ACCENT)}
            all_initial_words.append(new_word)
            #  ambiguous word       compound     monosyllabic     no stress mark or multiple stress marks
            if xml.attrib['obs'] or '_' in ws or '-' not in ws or u8n(ws).count(ACCENT) != 1:
                continue
            x += 1
            selected_words.append(new_word)
            # word_dict[xml.attrib['w']] = ws
            # print(xml.attrib['w'], xml.text)
            if x == max_examples:
                break
    print(x, i)
    return selected_words, all_initial_words


# la ce se referă partea cu raw data? -> la faptul că semivocalele pot fi tratate uneori ca niște consoane


def syllable_counts(all_words):
    b_counts_all = 5 * [0]
    e_counts_all = 5 * [0]
    for w in all_words:
        syl = w['text'].split('-')
        for i in range(min(5, len(syl))):
            b_counts_all[i] += u8n(syl[i]).count(ACCENT)
            e_counts_all[i] += u8n(syl[len(syl) - i - 1]).count(ACCENT)

    # good counts
    b_percent = [100 * count / len(all_words) for count in b_counts_all]
    e_percent = [100 * count / len(all_words) for count in e_counts_all]

    return b_percent, e_percent


def make_tags(word, use2=True):
    pos = 0
    tags = []
    wrd = ''.join(c for c in word if c != '-')
    wrd = u8n(wrd)
    for i in range(len(wrd)):
        if (i + 1 < len(wrd) and wrd[i + 1] == ACCENT) or (
                i + 2 < len(wrd) and wrd[i + 1] in DIACRITICS and wrd[i + 2] == ACCENT):
            if len(tags) > 0 and tags[len(tags) - 1] == 0 or len(tags) == 0:
                tags.append(1)
            if use2:
                pos = 2
            continue
        if wrd[i] in DIACRITICS or wrd[i] == ACCENT:
            continue
        tags.append(pos)
    return tags


def make_syllable_tags(word):
    tags = []
    syllables = word.split('-')
    nr = len(syllables)
    for i, syllable in enumerate(syllables):
        n = len(syllable)
        if i == 0:
            # first syllable
            bound = 0
        elif i < nr - 1:
            # "middle" syllable: for these, we have something like
            # 0 1 ... k-1 k k-1 ... 1 0
            # or
            # 0 1 ... k-1 k k k-1 ... 1 0
            bound = n / 2
        else:
            # last syllable
            bound = n
        for pos in range(n - 1):
            if pos < bound:
                tags.append(pos + 1)
            else:
                tags.append(n - pos - 1)
        if i < nr - 1:
            tags.append(0)

    return tags


def _cv(c):
    return '' if c == '-' or c in DIACRITICS or c == ACCENT else ('v' if c in 'aeiou' else 'c')


def _cv2(c):
    return '-' if c == '-' else (
        '' if c in DIACRITICS else (
            's' if c == ACCENT else ('v' if c in 'aeiou' else 'c')))


def make_cv(word):
    return ''.join(_cv(c) for c in u8n(word))


def make_cv2(word):
    return ''.join(_cv2(c) for c in u8n(word))


# cv_words = [make_cv(w['w']) for w in words]  # to be added


def feats_for_char(word, i, max_grams, window=2, word_ngrams=False, cv_ngrams=True):
    if word[i] == '-' or word[i] in DIACRITICS or word[i] == ACCENT:
        return ''

    h = syllable_markers_feats(i, word)

    wrd = ''.join(c for c in word if c != '-')
    i -= word[:i + 1].count('-')
    if word_ngrams:
        add_ngrams(h, i, max_grams, window, wrd, prefix='c')
    if cv_ngrams:
        cv_structure = make_cv(wrd)
        add_ngrams(h, i, max_grams, window, cv_structure, prefix='cv')

    return '\t'.join(h)


def syllable_markers_feats(i, word, s_char='-'):
    h = []
    n = len(word)
    h.extend([
        f'cbs=' + ('1' if i < n - 1 and word[i + 1] == s_char else '0'),  # exactly before split
        f'cas=' + ('1' if len(word) > i > 0 and word[i - 1] == s_char else '0'),  # exactly after split
        f'c1l=' + ('1' if word[:i + 1].count(s_char) == 0 else '0'),  # first syllable counting from left to right
        f'c2l=' + ('1' if word[:i + 1].count(s_char) == 1 else '0'),  # second syllable counting from left to right
        f'c3l=' + ('1' if word[:i + 1].count(s_char) == 2 else '0'),  # third syllable counting from left to right
        f'c4l=' + ('1' if word[:i + 1].count(s_char) == 3 else '0'),  # fourth syllable counting from left to right
        f'c1r=' + ('1' if word[i:].count(s_char) == 0 else '0'),  # first syllable counting from right to left
        f'c2r=' + ('1' if word[i:].count(s_char) == 1 else '0'),  # second syllable counting from right to left
        f'c3r=' + ('1' if word[i:].count(s_char) == 2 else '0'),  # third syllable counting from right to left
        f'c4r=' + ('1' if word[i:].count(s_char) == 3 else '0')])  # fourth syllable counting from right to left
    return h


def add_ngrams(h, i, max_grams, window, wrd, prefix):
    n2 = len(wrd)
    h.extend(list({
        f'{prefix}{max(i + win, 0)}_'
        f'{min(max(0, i + win + ng), i + window + 1, n2 - 1)}='
        f'{wrd[max(i + win, 0):min(max(0, i + win + ng), i + window + 1, n2 - 1)]}'
        for ng in range(1, max_grams + 1)
        for win in range(-window, window + 1)
        if wrd[max(i + win, 0): min(max(0, i + win + ng), i + window + 1, n2 - 1)] != ''}))


def syllable_ngrams_stress(word, i, max_grams, window=2):
    if word[i] == '-':
        return ''

    h = []
    syllables = word.split('-')
    i = word[:i + 1].count('-')
    n2 = len(syllables)
    h.extend(list({
        f'g{max(i + win, 0)}_{min(max(0, i + win + ng), i + window + 1, n2)}='
        f'{"".join(syllables[max(i + win, 0):min(max(0, i + win + ng), i + window + 1, n2)])}'
        for ng in range(1, max_grams + 1)
        for win in range(-window, window + 2)
        if syllables[max(i + win, 0): min(max(0, i + win + ng), i + window + 1, n2)] != []}))

    return '\t'.join(h)


def char_ngrams_syllables(word, i, max_grams, window=2):
    h = []
    wrd = ''.join(c for c in word if c != '-')
    # we might not actually care about hyphens, since these should not be available
    hyphens = word[:i + 1].count('-')
    i -= hyphens

    for ws in range(-window + 1, window + 1):
        for ng in range(1, max_grams + 1):
            left = max(0, ws + i + hyphens)
            right = min(max(0, ws + ng + i + hyphens), len(wrd), window + 1 + i + hyphens)
            if wrd[left:right] != '':
                h.append(
                    f'g{left}_{right}={wrd[left:right]}'
                )

    h = set(h)
    return '\t'.join(h)


def write_stress_features(words_list, name, max_grams=4, window=4, include_word_ngrams=True):
    use2 = True
    with open(
            os.path.join(
                'feats',
                f'{name}_stress_features_{max_grams}-grams_w{window}_'
                f'{"w" if include_word_ngrams else ""}_{"u2" if use2 else ""}.txt'
            ),
            mode='w',
            encoding=ENCODING
    ) as f:
        for word in words_list:
            tags = make_tags(word['text'], use2)
            j = 0
            for i, c in enumerate(u8n(word['x'])):
                if c in DIACRITICS or c == ACCENT:
                    continue
                feats = feats_for_char(word['x'], i, max_grams, window, word_ngrams=include_word_ngrams)
                if len(feats) > 0:
                    f.write(f'{tags[j]}\t{feats}\n')
                    j += 1
            f.write('\n')


def write_syllable_features(words_list, name, max_grams=4, window=4):
    with open(
            os.path.join('feats', f'{name}_syllable_features_{max_grams}-grams_w{window}.txt'),
            mode='w',
            encoding=ENCODING
    ) as f:
        for word in words_list:
            if word['x'] == '':
                print(f'warning: empty word: {word["w"]}')
                continue
            tags = make_syllable_tags(word['x'])
            j = 0
            wrd = ''.join(c for c in word['w'][:-1] if c is not '-')
            for i, c in enumerate(wrd):
                feats = char_ngrams_syllables(word['w'], i, max_grams, window)
                if len(feats) > 0:
                    f.write(f'{tags[j]}\t{feats}\n')
                    j += 1
            f.write('\n')


def write_data():
    words, _ = read_data()
    test_size = 0.2
    words_train, words_test = train_test_split(words, test_size=test_size, random_state=42)
    write_stress_features(words_train, f"train_{round(1 - test_size, 2)}")
    write_stress_features(words_test, f"test_{test_size}")
    # write_syllable_features(words_train, f"train_{round(1-test_size, 2)}")
    # write_syllable_features(words_test, f"test_{test_size}")


def compute_baseline(words, language='rsd', cv_func1=make_cv, cv_func2=make_cv2):
    test_size = 0.2
    words_train, words_test = train_test_split(words, test_size=test_size, random_state=42)
    cv_train = [[word['text'], cv_func1(word['text']), cv_func2(word['text'])] for word in words_train]
    cv_test = [[word['text'], cv_func1(word['text']), cv_func2(word['text'])] for word in words_test]
    counts = {}
    for word, cv1, cv2 in cv_train:
        if cv1 not in counts:
            counts[cv1] = {}
        if cv2 not in counts[cv1]:
            counts[cv1][cv2] = 1
        else:
            counts[cv1][cv2] += 1
    for k, v in counts.items():
        counts[k] = dict(sorted(counts[k].items(), key=lambda x: -x[1]))
    nr = 0
    nr2 = 0
    total = 0
    for _, cv1, cv2 in cv_test:
        total += 1
        if cv1 in counts:
            if list(counts[cv1].keys())[0] == cv2:
                nr += 1
        else:
            nr2 += 1
    print(language)
    print(len(counts))
    print(nr / total)
    print(nr, nr2, total)
    print((nr + nr2) / total)
    return counts, cv_test
    # In[2]: import rsd_crf_features as rcf
    # 0.23.2
    # 11.0.0
    # In[3]: words, _ = rcf.read_data()
    # 500347 525530
    # In[4]: cn, _ = rcf.compute_baseline(words)
    # rsd
    # 9449
    # 0.4074947536724293
    # 40778 797 100070
    # 0.4154591785749975


if __name__ == "__main__":
    write_data()
    # words, _ = read_data(10000)
    # compute_baseline(words)
