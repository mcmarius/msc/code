$(document).ready(function () {
    let pred_func = function () {
        const text = $("#input-text").val();
        const lang = $("#languages").val();
        // https://stackoverflow.com/questions/40963401/flask-dynamic-data-update-without-reload-page
        // https://learn.jquery.com/using-jquery-core/faq/how-do-i-get-the-text-value-of-a-selected-option/
        // http://css-plus.com/2010/03/6-steps-to-take-if-your-jquery-is-not-working/
        // reminder: do not forget document ready...

        $.ajax({
            url: "/predict",
            type: "get",
            data: {text: text, lang: lang},
            success: function (response) {
                $("#prediction").html(response);
            },
            error: function (xhr) {
                alert("Error");
                $("#prediction").html(xhr);
            }
        });
    };

    $("#input-text").change(pred_func);
    $("#languages").change(pred_func);
});