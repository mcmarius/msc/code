import subprocess

from flask import Flask, render_template, request
from ipapy.ipastring import IPAString
from werkzeug.middleware.proxy_fix import ProxyFix
from markupsafe import Markup

import epitran
import pyphen

import rsd_crf_features as rcf
import wiktionary_crf_features as wcf
from preprocess import wiktionary as pw

app = Flask(__name__)
app.wsgi_app = ProxyFix(app.wsgi_app)


@app.route("/")
def hello_world(name=None):
    return render_template('index.jinja2', name=name)


# https://stackoverflow.com/questions/40963401/flask-dynamic-data-update-without-reload-page
def add_stress_marks(words, stress_marks, ipa=False):
    syllable_mark = '-'
    key = 'x'
    normalize = True
    if ipa:
        syllable_mark = pw.IPA_SYLLABLE_MARK
        key = 'ipa'
        normalize = False
    new_words = []
    for i, wrd in enumerate(words):
        word = wrd[key]
        if wrd[key].count(syllable_mark) < 1:
            new_words.append(word)
            print("skip")
            continue
        labels = stress_marks[i].split("\n")
        labels = [label for label in labels if label is not '']
        print(f"L: {labels}")
        try:
            stress_pos = labels.index('1')
        except ValueError:
            new_words.append(word)
            print("skip2")
            continue
        print(f"pos: {stress_pos}")
        new_word = []
        j = 0
        if normalize:
            word = rcf.u8n(word)
        for c in word:
            if c == syllable_mark or c in rcf.DIACRITICS:  # or not IPAString(unicode_string=c).ipa_chars[0].is_letter:
                new_word.append(c)
                continue
            elif j == stress_pos:
                new_word.append(pw.IPA_STRESS_MARK)
            new_word.append(c)
            j += 1
        new_words.append(''.join(new_word))
    return new_words


EPITRAN_CODE = {
    'es_ES': 'spa-Latn',
    'de_DE': 'deu-Latn',
    'it_IT': 'ita-Latn',
    'pt_PT': 'por-Latn',
    'ro_RO': 'ron-Latn',
    'ru_RU': 'rus-Cyrl',
}


def make_ipa(word, dict_lang):
    ipa_str = epitran.Epitran(EPITRAN_CODE[dict_lang]).transliterate(word)
    print(f"e: {ipa_str}")
    return IPAString(unicode_string=ipa_str, ignore=True)


@app.route("/predict")
def predict():
    text = request.args.get('text', '')
    lang = request.args.get('lang', '')
    prediction_text = ""
    if text is not '' and lang is not '':
        prediction_text = Markup("Result<br>")  # Markup(f"Country code: {lang}<br>\n")
        dict_lang = lang
        if lang == 'rsd':
            dict_lang = 'ro_RO'
        words = text.split()
        if lang == 'rsd':
            dic = pyphen.Pyphen(
                lang=dict_lang,
                left=pw.LANG_DATA[dict_lang].get('left_h_min', 2) or 2,
                right=pw.LANG_DATA[dict_lang].get('right_h_min', 2) or 2
            )
            words2 = []
            for word in words:
                if '-' not in word:
                    words2.append([word, dic.inserted(word)])
                else:
                    w = ''.join(c for c in word if c is not '-')
                    words2.append([w, word])
            # [[w, dic.inserted(w)] for w in words]
            words = words2
            print(words)
            words = [{'w': w[0], 'x': ''.join(c for c in rcf.u8n(w[1])), 'text': w[1]} for w in words]
            rcf.write_stress_features(words, "user_input")
            result = subprocess.run(["./bin/crfsuite", "tag",
                                     "-m", "models/char_rsd_0.8_stress_4-grams_w4_w_i50.model",
                                     "-t", "feats/user_input_stress_features_4-grams_w4_w_u2.txt"],
                                    stdout=subprocess.PIPE,
                                    text=True)
            stress_marks = result.stdout.split("\nPerformance")[0].split("\n\n")
            words2 = add_stress_marks(words, stress_marks)
            print(stress_marks)
            prediction_text += ' '.join(words2)
        else:
            if dict_lang not in EPITRAN_CODE.keys():
                print(f"hm..{words}")
                check = ["|" in word for word in words]
                if False not in check:
                    words = [
                        pw.add_syllable_marks(dict_lang,
                                              IPAString(unicode_string=word.split("|")[0], ignore=True),
                                              word.split("|")[1], skip_stress=True)
                        for word in words]
                else:
                    prediction_text = "Warning! IPA transliteration not supported for this language." \
                                      " Please also insert IPA strings: IPA|word"
                    return render_template('predict.jinja2', pred=prediction_text)
                    # prediction_text += Markup("<br>")
            else:

                words = [pw.add_syllable_marks(dict_lang,
                                               make_ipa(word, dict_lang),
                                               word,
                                               skip_stress=True)
                         for word in words]
            print(words)
            words = [w for w in words if w is not None]
            words2 = [{'ipa': w} for w in words]
            wcf.write_ipa_stress_features(words2, dict_lang, "user_input", overwrite=True)
            result = subprocess.run(["./bin/crfsuite", "tag",
                                     "-m", f"models/ipa_{dict_lang}_0.8_stress_4-grams_w4_w_i50.model",
                                     "-t", f"feats/user_input_{dict_lang}_ipa_stress_features_4-grams_w4_w_u2.txt"],
                                    stdout=subprocess.PIPE,
                                    text=True)
            stress_marks = result.stdout.split("\nPerformance")[0].split("\n\n")
            print(stress_marks)
            words3 = add_stress_marks(words2, stress_marks, ipa=True)
            prediction_text += f"/{' '.join(words3)}/"
    return render_template('predict.jinja2', pred=prediction_text)


# https://ankane.org/git-lfs-on-heroku
