import json
import os
from typing import List

from ipapy import is_valid_ipa
from ipapy.ipachar import IPAChar
from ipapy.ipastring import IPAString

import pyphen

from constants import ENCODING

IPA_STRESS_MARK = "ˈ"
IPA_SYLLABLE_MARK = "."
SYLLABLE_MARK = '-'
SEMIVOWEL_MARK = '̯'

# These should be Europarl corpus languages with unpredictable stress (plus Russian)
# Language codes used by pyphen
LANG_DATA = {
    'bg_BG': {'vowels': 'еиаъоуАЕИОУЪ',  'left_h_min': 2, 'right_h_min': 2},
    'da_DK': {'vowels': 'aeiouAEIOU', 'left_h_min': 1, 'right_h_min': 1},    # Danish
    'de_DE': {'vowels': 'aeiouAEIOU', 'left_h_min': 2, 'right_h_min': 2},    # German
    'el_GR': {'vowels': 'ΑαΕεΙιΟοΥυΩω', 'left_h_min': 1, 'right_h_min': 1},  # Greek
    'es':    {'vowels': 'aeiouAEIOU', 'left_h_min': 2, 'right_h_min': 2},
    'es_ES': {'vowels': 'aeiouAEIOU', 'left_h_min': 2, 'right_h_min': 2},
    'en_US': {'vowels': 'aeiouAEIOU', 'left_h_min': 2, 'right_h_min': 3},
    'it_IT': {'vowels': 'aeiouAEIOU', 'left_h_min': 1, 'right_h_min': 1},
    'lt_LT': {'vowels': 'aeiouAEIOU', 'left_h_min': 1, 'right_h_min': 1},       # Lithuanian
    'nb_NO': {'vowels': 'aeiouAEIOU', 'left_h_min': 1, 'right_h_min': 1},    # Norvegian Bokmål
    'nn_NO': {'vowels': 'aeiouAEIOU', 'left_h_min': 1, 'right_h_min': 1},    # Norvegian Nynorsk
    'pt_PT': {'vowels': 'aeiouAEIOU', 'left_h_min': 1, 'right_h_min': 1},
    'ro_RO': {'vowels': 'aeiouăâîAEIOUĂÂÎ', 'left_h_min': 1, 'right_h_min': 1},
    'ru_RU': {'vowels': 'иыэеаяоёую', 'left_h_min': 1, 'right_h_min': 1},
    'sl_SL': {'vowels': '', 'left_h_min': 2, 'right_h_min': 2},  # Slovenian
    'sv_SE': {'vowels': 'aeiouAEIOU', 'left_h_min': 1, 'right_h_min': 2},    # Swedish
}


def write_stress_file(file_name):
    file_name_root = file_name.split(".json")[0]
    dest_file_name = file_name_root + "_stress.txt"
    dest_file_name = os.path.join("datasets", "wiktionary", dest_file_name)
    if os.path.isfile(dest_file_name):
        print(f"Warning: file {dest_file_name} already exists. Skipping...")
        return

    with open(os.path.join("datasets", "wiktionary", file_name), mode="r", encoding=ENCODING) as f:
        with open(dest_file_name, mode="w", encoding=ENCODING) as out:
            i = j = skipped = 0
            all_words = set()
            for line in f:
                entry = json.loads(line)
                if entry['word'][0] == SYLLABLE_MARK:
                    # ignore suffixes
                    continue
                # if i > 5000:
                #     break
                sounds = entry.get('sounds')
                if sounds is None:
                    continue

                for sound in sounds:
                    value = sound.get('ipa')
                    if value is None:
                        continue
                    if value[0] != '/' and value[-1] != '/' and value[0] != '[' and value[-1] != ']':
                        continue

                    word_sounds = [w for w in value[1:-1].split(" ") if len(w) > 0]
                    word_repr = entry.get('word')
                    # if " " in word_repr:
                    words = word_repr.split(" ")
                    # else:
                    #     words = word_repr.split("-")
                    words = [w for w in words if len(w) > 0]
                    if len(words) != len(word_sounds):
                        # print(words, word_sounds)
                        skipped += 1
                        continue
                    for k, word_sound in enumerate(word_sounds):
                        if not is_valid_ipa(word_sound):
                            continue
                        ipa = IPAString(unicode_string=word_sound)
                        has_stress = False
                        for char in ipa:
                            if "primary-stress" in char.name:
                                has_stress = True
                                break

                        if has_stress:
                            lang = file_name_root.split("dictionary-")[1]
                            new_word = add_syllable_marks(lang, ipa, words[k])
                            if new_word:
                                all_words.add(new_word)
                                j += 1
                i += 1
            # todo write in batches?
            print(len(all_words))
            out.writelines(sorted(all_words))

    print(i, j, skipped)


def add_syllable_marks(lang: str, ipa: IPAString, word: str, skip_stress=False):
    dic = pyphen.Pyphen(
        lang=lang,
        left=LANG_DATA[lang].get('left_h_min', 2) or 2,
        right=LANG_DATA[lang].get('right_h_min', 2) or 2
    )
    hyphen = dic.inserted(word)
    new_ipa_chars = []
    new_chars = []
    ih = 0
    bogus = False
    ipa_syllable = IPAChar(["syllable-break"], unicode_repr=IPA_SYLLABLE_MARK)
    for ic, char in enumerate(ipa):
        if "stress" in char.name:
            if 0 < ih < len(hyphen):
                if hyphen[ih] != SYLLABLE_MARK:
                    bogus = True
                    break
                new_chars.append(hyphen[ih])
                new_ipa_chars.append(ipa_syllable)
                ih += 1
            new_ipa_chars.append(char)
            if "primary-stress" in char.name:
                new_chars.append(IPA_STRESS_MARK)
            continue
        elif "diacritic" in char.name:
            new_ipa_chars.append(char)
            if ih < len(hyphen) and hyphen[ih] == 'ь':
                new_chars.append(hyphen[ih])
                ih += 1
            continue
        elif len(new_ipa_chars) > 0 and ih < len(hyphen) and hyphen[ih] == SYLLABLE_MARK and \
                (ic > 0 and "stress" not in ipa.ipa_chars[ic - 1].name or
                 ic + 1 < len(ipa.ipa_chars) and "stress" not in ipa.ipa_chars[ic + 1].name):
            if "syllable-break" not in char.name:
                new_ipa_chars.append(ipa_syllable)
            new_chars.append(hyphen[ih])
            ih += 1
        elif "syllable-break" in char.name:
            if ih < len(hyphen) and hyphen[ih] != SYLLABLE_MARK:
                # IPA syllabication is inconsistent with the dictionary one: abort
                bogus = True
                break
            new_ipa_chars.append(char)
            new_chars.append(SYLLABLE_MARK)
            continue

        new_ipa_chars.append(char)
        if char.is_letter:
            if ih < len(hyphen):
                new_chars.append(hyphen[ih])
                ih += 1
    if bogus:
        return None

    while ih < len(hyphen):
        new_chars.append(hyphen[ih])
        ih += 1

    ipa_with_syllables = ''.join(c.unicode_repr for c in new_ipa_chars)
    if skip_stress:
        return ipa_with_syllables

    if IPA_SYLLABLE_MARK not in ipa_with_syllables or SYLLABLE_MARK not in new_chars:
        return None

    result = move_stress_on_vowel(ipa_with_syllables, new_chars)
    if result is None:
        return None

    ipa_marks, char_marks = result

    return f"{ipa_marks}\t{char_marks}\t{word}\t{ipa_with_syllables}\t{''.join(new_chars)}\n"


def move_stress_on_vowel(ipa_str: str, chars: List[str]):
    # semivowels = ['j', 'ɥ', 'ɰ', 'w']
    ipa_syllable = syllable = None
    ipa_idx = 0
    ipa_chars = ipa_str.split(IPA_SYLLABLE_MARK)
    for x in ipa_chars:
        if IPA_STRESS_MARK in x:
            ipa_syllable = x[1:]  # discard leading stress mark
            break
        ipa_idx += 1
    idx = 0
    initial_chars = ''.join(chars).split(SYLLABLE_MARK)
    for x in initial_chars:
        if IPA_STRESS_MARK in x:
            syllable = x[1:]  # discard leading stress mark
            break
        idx += 1
    if ipa_syllable is None or syllable is None:
        return None

    ipa_syllable = IPAString(unicode_string=ipa_syllable)
    i = j = 0
    k = 0
    pos = -1
    char_pos = -1
    for ipa_char in ipa_syllable:
        if ipa_char.is_consonant:
            j += 1
        elif ipa_char.is_vowel:
            if i+1 < len(ipa_syllable):
                if ipa_syllable[i+1].unicode_repr != SEMIVOWEL_MARK:
                    k += 1
                    pos = i
                    char_pos = j
            else:
                k += 1
                pos = i
                char_pos = j
            j += 1
        i += 1

    if k != 1:
        return None

    new_chars = [c for c in syllable]
    if pos > 0:
        new_chars.insert(char_pos, IPA_STRESS_MARK)
        ipa_syllable.ipa_chars.insert(pos, IPAChar('primary stress', unicode_repr=IPA_STRESS_MARK))
        ipa_chars[ipa_idx] = ''.join(c.unicode_repr for c in ipa_syllable.ipa_chars)
        initial_chars[idx] = ''.join(new_chars)

    return IPA_SYLLABLE_MARK.join(ipa_chars), SYLLABLE_MARK.join(initial_chars)


if __name__ == "__main__":
    write_stress_file("kaikki.org-dictionary-bg_BG.json")

# notes: no stress: French, Mandarin/Chinese
#        predictable/fixed stress: Finnish, Hungarian, Czech, Icelandic,
#                                  Armenian, Polish, Latin, Arabic, Latvian,
#                                  Estonian, Dutch?, Slovak

# could try: Spanish (68k), Italian (13k), Portuguese (13k + 150k), English (??), Russian (needs syllabication -> 291k)
# Romanian: 10943 -> 8545
# too few examples: German (1.7k - needs syllabication -> 32k), Bulgarian (30k after syllabification)
