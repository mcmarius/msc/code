import unittest

import wiktionary_crf_features as wcf


class Helpers(unittest.TestCase):
    def test_make_tags(self):
        self.assertEqual([1, 0, 0, 0, 0, 0], wcf.make_tags('ˈan-co-ră'))
        self.assertEqual([1, 0, 0, 0], wcf.make_tags('ˈîn-că'))
        self.assertEqual([0, 1, 0, 0, 0], wcf.make_tags('bˈan-dă'))
        self.assertEqual([0, 0, 0, 0, 0, 0, 1, 0, 0], wcf.make_tags('o-ră-că-iˈa-lă'))
        self.assertEqual([0, 0, 0, 0, 0, 1], wcf.make_tags('a-run-cˈa'))
        self.assertEqual([0, 0, 0, 0, 0, 1], wcf.make_tags('co-bo-rˈî'))

        self.assertEqual([1, 2, 2, 2, 2, 2], wcf.make_tags('ˈan-co-ră', use2=True))
        self.assertEqual([1, 2, 2, 2], wcf.make_tags('ˈîn-că', use2=True))
        self.assertEqual([0, 1, 2, 2, 2], wcf.make_tags('bˈan-dă', use2=True))
        self.assertEqual([0, 0, 0, 0, 0, 0, 1, 2, 2], wcf.make_tags('o-ră-că-iˈa-lă', use2=True))
        self.assertEqual([0, 0, 0, 0, 0, 1], wcf.make_tags('a-run-cˈa', use2=True))
        self.assertEqual([0, 0, 0, 0, 0, 1], wcf.make_tags('co-bo-rˈî', use2=True))

    def test_make_ipa_tags(self):
        self.assertEqual([0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0], wcf.make_ipa_tags("mə.rɐ.zʲˈilʲ.nʲɪ.kəx"))
        self.assertEqual([0, 0, 0, 0, 0, 1, 2, 2, 2, 2, 2, 2], wcf.make_ipa_tags("mə.rɐ.zʲˈilʲ.nʲɪ.kəx", use2=True))

    def test_make_ipa_syllable_tags(self):
        self.assertEqual([3, 2, 1, 0, 1], wcf.make_ipa_syllable_tags('ʃko̯ˈa.lə'))
        # this example is just for testing, real IPA for this word seems different
        self.assertEqual([1, 0, 0, 1, 2, 3], wcf.make_ipa_syllable_tags('daˈ.ɪ.mənd'))


class CharFeatures(unittest.TestCase):
    def test_ro_bi_grams(self):
        expected = [
            'cas=0', 'cbs=1',
            'c1l=1', 'c2l=0', 'c3l=0', 'c4l=0',
            'c1r=0', 'c2r=0', 'c3r=0', 'c4r=0',
            'c0_1=a', 'c1_2=a', 'c2_3=l',  # 1-grams
            'c0_2=aa', 'c1_3=al',
            'cv0_1=v', 'cv1_2=v', 'cv2_3=c',  # 1-grams
            'cv0_2=vv', 'cv1_3=vc',
        ]
        expected.sort()
        actual = wcf.feats_for_char('a-a-le-ni-an', i=0, max_grams=2, lang='ro_RO', window=2, word_ngrams=True)\
            .split('\t')
        actual.sort()
        self.assertListEqual(expected, actual)

        expected = [
            'cas=0', 'cbs=1',
            'c1l=1', 'c2l=0', 'c3l=0', 'c4l=0',
            'c1r=0', 'c2r=1', 'c3r=0', 'c4r=0',
            'c1_2=k', 'c2_3=o', 'c3_4=a', 'c4_5=l',  # 1-grams
            'c1_3=ko', 'c2_4=oa', 'c3_5=al',
            'cv1_2=c', 'cv2_3=v', 'cv3_4=v', 'cv4_5=c',  # 1-grams
            'cv1_3=cv', 'cv2_4=vv', 'cv3_5=vc',
        ]
        expected.sort()
        actual = wcf.feats_for_char(
            'ʃko̯a.lə', i=3, max_grams=2, lang='ro_RO', s_mark=wcf.IPA_SYLLABLE_MARK, window=2, word_ngrams=True)\
            .split('\t')
        actual.sort()
        self.assertListEqual(expected, actual)

        self.assertEqual(
            '',
            wcf.feats_for_char('a-a-le-ni-an', i=1, max_grams=2, lang='ro_RO', window=2)
        )

        expected = [
            'cas=0', 'cbs=1',
            'c1l=1', 'c2l=0', 'c3l=0', 'c4l=0',
            'c1r=0', 'c2r=0', 'c3r=0', 'c4r=0',
            'cv0_1=v', 'cv1_2=v', 'cv2_3=c',  # 1-grams
            'cv0_2=vv', 'cv1_3=vc',
        ]
        expected.sort()
        actual = wcf.feats_for_char('a-a-le-ni-an', i=0, max_grams=2, lang='ro_RO', window=2).split('\t')
        actual.sort()
        self.assertListEqual(expected, actual)

    def test_ro_tri_grams(self):
        expected = [
            'cas=0', 'cbs=1',
            'c1l=1', 'c2l=0', 'c3l=0', 'c4l=0',
            'c1r=0', 'c2r=0', 'c3r=0', 'c4r=0',
            'c0_1=a', 'c1_2=a', 'c2_3=l',
            'c0_2=aa', 'c1_3=al',
            'c0_3=aal',
            'cv0_1=v', 'cv1_2=v', 'cv2_3=c',
            'cv0_2=vv', 'cv1_3=vc',
            'cv0_3=vvc'
        ]
        expected.sort()
        actual = wcf.feats_for_char('a-a-le-ni-an', i=0, max_grams=3, lang='ro_RO', window=2, word_ngrams=True)\
            .split('\t')
        actual.sort()
        self.assertListEqual(expected, actual)


if __name__ == '__main__':
    unittest.main()
