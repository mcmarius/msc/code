import unittest

from ipapy.ipastring import IPAString

import preprocess.wiktionary as pw


class SyllableMarks(unittest.TestCase):
    def test_add_syllable_marks_ro(self):
        ipa = IPAString(unicode_string='ˈamsterdam')
        self.assertEqual(
            "ˈam.ster.dam\tˈAm-ster-dam\tAmsterdam\tˈam.ster.dam\tˈAm-ster-dam\n",
            pw.add_syllable_marks('ro_RO', ipa, 'Amsterdam')
        )
        ipa = IPAString(unicode_string='abˈsent')
        self.assertEqual(
            "ab.sˈent\tab-sˈent\tabsent\tab.ˈsent\tab-ˈsent\n",
            pw.add_syllable_marks('ro_RO', ipa, 'absent')
        )
        ipa = IPAString(unicode_string='d͡ʒeneˈral')
        self.assertEqual(
            "d͡ʒe.ne.rˈal\tge-ne-rˈal\tgeneral\td͡ʒe.ne.ˈral\tge-ne-ˈral\n",
            pw.add_syllable_marks('ro_RO', ipa, 'general')
        )
        ipa = IPAString(unicode_string='deˈkade')
        self.assertEqual(
            "de.kˈa.de\tde-cˈa-de\tdecade\tde.ˈka.de\tde-ˈca-de\n",
            pw.add_syllable_marks('ro_RO', ipa, 'decade')
        )
        ipa = IPAString(unicode_string='ˌuŋɡuˈreʃtʲ')
        self.assertEqual(
            "ˌuŋ.ɡu.rˈeʃtʲ\tun-gu-rˈești\tungurești\tˌuŋ.ɡu.ˈreʃtʲ\tun-gu-ˈrești\n",
            pw.add_syllable_marks('ro_RO', ipa, 'ungurești')
        )
        ipa = IPAString(unicode_string='ˌt͡ʃiviliˈzat͡si.e')
        self.assertEqual(
            "ˌt͡ʃi.vi.li.zˈa.t͡si.e\tci-vi-li-zˈa-ți-e\tcivilizație\tˌt͡ʃi.vi.li.ˈza.t͡si.e\tci-vi-li-ˈza-ți-e\n",
            pw.add_syllable_marks('ro_RO', ipa, 'civilizație')
        )

    def test_add_syllable_marks_de(self):
        ipa = IPAString(unicode_string='ˈvaɪ̯nˌbʁɛndə')
        self.assertEqual(
            "vˈaɪ̯n.ˌbʁɛn.də\tWˈein-brän-de\tWeinbrände\tˈvaɪ̯n.ˌbʁɛn.də\tˈWein-brän-de\n",
            pw.add_syllable_marks('de_DE', ipa, 'Weinbrände')
        )

    def test_add_syllable_marks_ru(self):
        ipa = IPAString(unicode_string='mərɐˈzʲilʲnʲɪkəx')
        self.assertEqual(
            "mə.rɐ.zʲˈilʲ.nʲɪ.kəx\tмо-ро-зˈиль-ни-ках\tморозильниках\tmə.rɐ.ˈzʲilʲ.nʲɪ.kəx\tмо-ро-ˈзиль-ни-ках\n",
            pw.add_syllable_marks('ru_RU', ipa, 'морозильниках')
        )


if __name__ == '__main__':
    unittest.main()
