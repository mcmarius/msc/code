import os
import unittest

import rsd_crf_features as rcf


class Helpers(unittest.TestCase):
    def test_make_cv(self):
        self.assertEqual('vccv', rcf.make_cv('î́n-că'))
        self.assertEqual('cvvcvcc', rcf.make_cv('dí-a-mond'))
        self.assertEqual('vcccccvcvcv', rcf.make_cv('opt-spre-ze-ce'))

    def test_u8n(self):
        from rsd_crf_features import u8n, ACCENT
        # not a great test since it uses u8n 3 times, but better than nothing
        # the idea is that we need to compare normalized strings
        self.assertEqual(
            ''.join(c for c in u8n('î́ncă') if c != ACCENT),
            u8n('încă')
        )

    def test_make_tags(self):
        self.assertEqual([0, 1, 2, 2, 2, 2, 2], rcf.make_tags('dí-a-mond', use2=True))
        self.assertEqual([0, 1, 0, 0, 0, 0, 0], rcf.make_tags('dí-a-mond', use2=False))
        self.assertEqual([1, 2, 2, 2], rcf.make_tags('î́n-că', use2=True))
        self.assertEqual([1, 0, 0, 0], rcf.make_tags('î́n-că', use2=False))
        # wds = [{'text': 'pa-ten-tá-ră', 'x': 'pa-ten-ta-ră'}, {'text': 'o-ră-că-iá-lă', 'x': 'o-ră-că-ia-lă'}]
        self.assertEqual([0, 0, 0, 0, 0, 0, 1, 2, 2], rcf.make_tags('pa-ten-tá-ră', use2=True))
        self.assertEqual([0, 0, 0, 0, 0, 0, 1, 0, 0], rcf.make_tags('pa-ten-tá-ră', use2=False))
        self.assertEqual([0, 0, 0, 0, 0, 0, 1, 2, 2], rcf.make_tags('o-ră-că-iá-lă', use2=True))
        self.assertEqual([0, 0, 0, 0, 0, 0, 1, 0, 0], rcf.make_tags('o-ră-că-iá-lă', use2=False))

    def test_make_syllable_tags(self):
        self.assertEqual([1, 0, 0, 1, 2, 3], rcf.make_syllable_tags('di-a-mond'))
        self.assertEqual([2, 1, 0, 1, 2, 1, 0, 1, 0, 1], rcf.make_syllable_tags('opt-spre-ze-ce'))


class CharFeatures(unittest.TestCase):
    # todo add some test cases for 3- and 4-grams
    def test_bi_grams(self):
        # todo add missing cases
        expected = [
            'cas=0', 'cbs=1',
            'c1l=1', 'c2l=0', 'c3l=0', 'c4l=0',
            'c1r=0', 'c2r=0', 'c3r=0', 'c4r=0',
            'c0_1=a', 'c1_2=a', 'c2_3=l',  # 1-grams
            'c0_2=aa', 'c1_3=al',
            'cv0_1=v', 'cv1_2=v', 'cv2_3=c',  # 1-grams
            'cv0_2=vv', 'cv1_3=vc',
        ]
        expected.sort()
        actual = rcf.feats_for_char('a-a-le-ni-an', i=0, max_grams=2, window=2, word_ngrams=True).split('\t')
        actual.sort()
        self.assertListEqual(expected, actual)
        self.assertEqual(
            '',
            rcf.feats_for_char('a-a-le-ni-an', i=1, max_grams=2, window=2)
        )

        expected = [
            'cas=0', 'cbs=1',
            'c1l=1', 'c2l=0', 'c3l=0', 'c4l=0',
            'c1r=0', 'c2r=0', 'c3r=0', 'c4r=0',
            'cv0_1=v', 'cv1_2=v', 'cv2_3=c',  # 1-grams
            'cv0_2=vv', 'cv1_3=vc',
        ]
        expected.sort()
        actual = rcf.feats_for_char('a-a-le-ni-an', i=0, max_grams=2, window=2).split('\t')
        actual.sort()
        self.assertListEqual(expected, actual)

    def test_tri_grams(self):
        expected = [
            'cas=0', 'cbs=1',
            'c1l=1', 'c2l=0', 'c3l=0', 'c4l=0',
            'c1r=0', 'c2r=0', 'c3r=0', 'c4r=0',
            'c0_1=a', 'c1_2=a', 'c2_3=l',
            'c0_2=aa', 'c1_3=al',
            'c0_3=aal',
            'cv0_1=v', 'cv1_2=v', 'cv2_3=c',
            'cv0_2=vv', 'cv1_3=vc',
            'cv0_3=vvc'
        ]
        expected.sort()
        actual = rcf.feats_for_char('a-a-le-ni-an', i=0, max_grams=3, window=2, word_ngrams=True).split('\t')
        actual.sort()
        self.assertListEqual(expected, actual)


class SyllableNGramsStress(unittest.TestCase):
    def test_bi_grams(self):
        expected = [
            'g0_1=ac', 'g1_2=com', 'g2_3=mo',
            'g0_2=accom', 'g1_3=commo',
        ]
        expected.sort()
        actual = rcf.syllable_ngrams_stress('ac-com-mo-da-tion', i=0, max_grams=2, window=2).split('\t')
        actual.sort()
        self.assertListEqual(expected, actual)

        actual = rcf.syllable_ngrams_stress('ac-com-mo-da-tion', i=1, max_grams=2, window=2).split('\t')
        actual.sort()
        self.assertListEqual(expected, actual)

        self.assertEqual('', rcf.syllable_ngrams_stress('ac-com-mo-da-tion', i=2, max_grams=2, window=2))

        expected = [
            'g0_1=ac', 'g1_2=com', 'g2_3=mo', 'g3_4=da',  # 'g4_5=tion',
            'g0_2=accom', 'g1_3=commo', 'g2_4=moda',  # 'g3_5=dation',
        ]
        expected.sort()
        actual = rcf.syllable_ngrams_stress('ac-com-mo-da-tion', i=3, max_grams=2, window=2).split('\t')
        actual.sort()
        self.assertListEqual(expected, actual)

        actual = rcf.syllable_ngrams_stress('ac-com-mo-da-tion', i=4, max_grams=2, window=2).split('\t')
        actual.sort()
        self.assertListEqual(expected, actual)

        actual = rcf.syllable_ngrams_stress('ac-com-mo-da-tion', i=5, max_grams=2, window=2).split('\t')
        actual.sort()
        self.assertListEqual(expected, actual)

        expected = [
            'g0_1=ac', 'g1_2=com', 'g2_3=mo', 'g3_4=da', 'g4_5=tion',
            'g0_2=accom', 'g1_3=commo', 'g2_4=moda', 'g3_5=dation',
        ]
        expected.sort()
        actual = rcf.syllable_ngrams_stress('ac-com-mo-da-tion', i=8, max_grams=2, window=2).split('\t')
        actual.sort()
        self.assertListEqual(expected, actual)

        expected = [
            'g2_3=mo', 'g3_4=da', 'g4_5=tion',
            'g2_4=moda', 'g3_5=dation',
        ]
        expected.sort()
        actual = rcf.syllable_ngrams_stress('ac-com-mo-da-tion', i=14, max_grams=2, window=2).split('\t')
        actual.sort()
        self.assertListEqual(expected, actual)


class CharNGramsSyllables(unittest.TestCase):
    def test_bi_grams(self):
        expected = [
            'g0_1=a', 'g1_2=c', 'g2_3=c',  # 1-grams
            'g0_2=ac', 'g1_3=cc',
        ]
        expected.sort()
        actual = rcf.char_ngrams_syllables('ac-com-mo-da-tion', i=0, max_grams=2, window=2).split('\t')
        actual.sort()
        self.assertListEqual(expected, actual)

        expected = [
            'g0_1=a', 'g1_2=c', 'g2_3=c', 'g3_4=o',
            'g0_2=ac', 'g1_3=cc', 'g2_4=co',
        ]
        expected.sort()
        actual = rcf.char_ngrams_syllables('ac-com-mo-da-tion', i=1, max_grams=2, window=2).split('\t')
        actual.sort()
        self.assertListEqual(expected, actual)

        expected = [
            'g1_2=c', 'g2_3=c', 'g3_4=o', 'g4_5=m',
            'g1_3=cc', 'g2_4=co', 'g3_5=om',
        ]
        expected.sort()
        actual = rcf.char_ngrams_syllables('ac-com-mo-da-tion', i=2, max_grams=2, window=2).split('\t')
        actual.sort()
        self.assertListEqual(expected, actual)

        expected = [
            'g2_3=c', 'g3_4=o', 'g4_5=m', 'g5_6=m',
            'g2_4=co', 'g3_5=om', 'g4_6=mm',
        ]
        expected.sort()
        actual = rcf.char_ngrams_syllables('ac-com-mo-da-tion', i=3, max_grams=2, window=2).split('\t')
        actual.sort()
        self.assertListEqual(expected, actual)

        expected = [
            'g3_4=o', 'g4_5=m', 'g5_6=m', 'g6_7=o',
            'g3_5=om', 'g4_6=mm', 'g5_7=mo',
        ]
        expected.sort()
        actual = rcf.char_ngrams_syllables('ac-com-mo-da-tion', i=4, max_grams=2, window=2).split('\t')
        actual.sort()
        self.assertListEqual(expected, actual)

        expected = [
            'g4_5=m', 'g5_6=m', 'g6_7=o', 'g7_8=d',
            'g4_6=mm', 'g5_7=mo', 'g6_8=od',
        ]
        expected.sort()
        actual = rcf.char_ngrams_syllables('ac-com-mo-da-tion', i=5, max_grams=2, window=2).split('\t')
        actual.sort()
        self.assertListEqual(expected, actual)

        expected = [
            'g5_6=m', 'g6_7=o', 'g7_8=d', 'g8_9=a',
            'g5_7=mo', 'g6_8=od', 'g7_9=da',
        ]
        expected.sort()
        actual = rcf.char_ngrams_syllables('ac-com-mo-da-tion', i=6, max_grams=2, window=2).split('\t')
        actual.sort()
        self.assertListEqual(expected, actual)

        expected = [
            'g9_10=t', 'g10_11=i', 'g11_12=o', 'g12_13=n',
            'g9_11=ti', 'g10_12=io', 'g11_13=on',
        ]
        expected.sort()
        actual = rcf.char_ngrams_syllables('ac-com-mo-da-tion', i=10, max_grams=2, window=2).split('\t')
        actual.sort()
        self.assertListEqual(expected, actual)

        expected = [
            'g10_11=i', 'g11_12=o', 'g12_13=n',
            'g10_12=io', 'g11_13=on',
        ]
        expected.sort()
        actual = rcf.char_ngrams_syllables('ac-com-mo-da-tion', i=11, max_grams=2, window=2).split('\t')
        actual.sort()
        self.assertListEqual(expected, actual)

        actual = rcf.char_ngrams_syllables('accommodation', i=11, max_grams=2, window=2).split('\t')
        actual.sort()
        self.assertListEqual(expected, actual)


@unittest.skipUnless(os.getenv('SLOW_TESTS'), 'slow')
class PaperCounts(unittest.TestCase):
    def test_syllable_counts(self):
        _, all_words = rcf.read_data()
        b_counts, e_counts = rcf.syllable_counts(all_words)
        delta = 1e-2
        self.assertAlmostEqual(b_counts[0], 5.59,  delta=delta)
        self.assertAlmostEqual(b_counts[1], 18.91, delta=delta)
        self.assertAlmostEqual(b_counts[2], 39.23, delta=delta)
        self.assertAlmostEqual(b_counts[3], 23.68, delta=delta)
        self.assertAlmostEqual(b_counts[4], 8.52,  delta=delta)

        self.assertAlmostEqual(e_counts[0], 28.16, delta=delta)
        self.assertAlmostEqual(e_counts[1], 43.93, delta=delta)
        self.assertAlmostEqual(e_counts[2], 24.14, delta=delta)
        self.assertAlmostEqual(e_counts[3], 3.08,  delta=delta)
        self.assertAlmostEqual(e_counts[4], 0.24,  delta=delta)


if __name__ == '__main__':
    unittest.main()
