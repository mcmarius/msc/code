# -*- coding: utf-8 -*-

# CRF features for RoSyllabiDict dataset

import os.path

from ipapy.ipachar import IPAChar
from ipapy.ipastring import IPAString
from sklearn.model_selection import train_test_split

from constants import ENCODING
from preprocess.wiktionary import IPA_STRESS_MARK, IPA_SYLLABLE_MARK, SYLLABLE_MARK, LANG_DATA
import rsd_crf_features as rcf


def read_data(language, max_examples=0):
    with open(
        os.path.join('datasets',
                     'wiktionary',
                     f'kaikki.org-dictionary-{language}_stress.txt'),
        mode='r',
        encoding=ENCODING
    ) as f:
        words = []
        for i, line in enumerate(f):
            if 0 < max_examples < i:
                break
            ipa, gt, word, *_ = line[:-1].split("\t")  # discard extra \n
            words.append({'w': word, 'text': gt, 'x': gt.replace(IPA_STRESS_MARK, ''), 'ipa': ipa})
        return words

# read_data('ro_RO', 10)


def vowels_wip(lang, n):
    dt = read_data(lang, n)
    vw = set()
    for w in dt:
        ipa = IPAString(unicode_string=w['ipa']).letters
        offset = 0
        for i, ch in enumerate(ipa):
            if i+offset < len(w['w']) and w['w'][i+offset] == 'ь' or ch.is_diacritic or ch.is_suprasegmental \
                    and ch.has_descriptor('long'):
                offset += 1
                continue
            if i+offset < len(w['w']) and w['w'][i+offset] not in vw and ch.is_vowel:
                print(i, w['w'], ipa, ch, w['w'][i+offset], len(w['w']))
                vw.add(w['w'][i + offset])
    return vw


def make_tags(word, use2=False):
    pos = 0
    tags = []
    i = -1
    while i+1 < len(word):
        i += 1
        if word[i] == SYLLABLE_MARK:
            continue
        if word[i] == IPA_STRESS_MARK:
            tags.append(1)
            if use2:
                pos = 2
            i += 1
        else:
            tags.append(pos)

    return tags


def make_ipa_tags(word, use2=False):
    pos = 0
    tags = []
    i = -1
    word = IPAString(unicode_string=word)
    chars = word.ipa_chars
    while i+1 < len(chars):
        i += 1
        if chars[i].is_diacritic or chars[i].unicode_repr == IPA_SYLLABLE_MARK:
            continue
        if chars[i].unicode_repr == IPA_STRESS_MARK:
            tags.append(1)
            if use2:
                pos = 2
            i += 1
        else:
            tags.append(pos)

    return tags


# note: word must NOT contain stress marks
def make_syllable_tags(word):
    return rcf.make_syllable_tags(word)


def make_ipa_syllable_tags(word):
    tags = []
    syllables = word.split(IPA_SYLLABLE_MARK)
    nr = len(syllables)
    for i, syllable in enumerate(syllables):
        syllable = IPAString(unicode_string=syllable)
        n = len(syllable.letters.ipa_chars)
        if i == 0:
            # first syllable
            bound = 0
        elif i < nr - 1:
            # "middle" syllable: for these, we have something like
            # 0 1 ... k-1 k k-1 ... 1 0
            # or
            # 0 1 ... k-1 k k k-1 ... 1 0
            bound = n / 2
        else:
            # last syllable
            bound = n
        for pos in range(n - 1):
            if pos < bound:
                tags.append(pos + 1)
            else:
                tags.append(n - pos - 1)
        if i < nr - 1:
            tags.append(0)

    return tags


def _cv(c, lang):
    return '' if c == '-' or c == IPA_STRESS_MARK else ('v' if c in LANG_DATA[lang]['vowels'] else 'c')


def make_cv(word, lang):
    return ''.join(_cv(c, lang) for c in word)


def _ipa_cv(char: IPAChar, with_stress=False):
    if char.is_vowel:
        return 'v'
    if char.is_consonant:
        return 'c'
    if with_stress and char.unicode_repr == IPA_STRESS_MARK:
        return 's'
    return ''


def make_ipa_cv(word: IPAString, with_stress=False):
    return ''.join(_ipa_cv(c, with_stress) for c in word.letters.ipa_chars)


def make_ipa_cv2(word: IPAString):
    return make_ipa_cv(word, True)


def feats_for_char(word, i, max_grams, lang, s_mark=SYLLABLE_MARK, window=2, word_ngrams=False, cv_ngrams=True):
    if word[i] == s_mark or word[i] == IPA_STRESS_MARK:
        return ''

    if s_mark == IPA_SYLLABLE_MARK:
        wrd = IPAString(unicode_string=word)
        wrd2 = ''.join(
            c.unicode_repr for c in wrd.ipa_chars if c.is_vowel or c.is_consonant or c.unicode_repr == s_mark)
        h = rcf.syllable_markers_feats(i, wrd2, s_mark)

    else:
        h = rcf.syllable_markers_feats(i, word, s_mark)
        wrd = ''.join(c for c in word if c != s_mark)
    i -= word[:i + 1].count(s_mark)
    if word_ngrams:
        if s_mark == IPA_SYLLABLE_MARK:
            add_ipa_ngrams(h, i, max_grams, window, wrd, prefix='c')
        else:
            rcf.add_ngrams(h, i, max_grams, window, wrd, prefix='c')
    if cv_ngrams:
        if s_mark == IPA_SYLLABLE_MARK:
            cv_structure = make_ipa_cv(wrd)
        else:
            cv_structure = make_cv(wrd, lang)
        rcf.add_ngrams(h, i, max_grams, window, cv_structure, prefix='cv')

    return '\t'.join(h)


def add_ipa_ngrams(h, i, max_grams, window, wrd: IPAString, prefix):
    n2 = len(wrd.letters.ipa_chars)
    new_values = set()
    for ng in range(1, max_grams + 1):
        for win in range(-window, window + 1):
            left = max(i + win, 0)
            right = min(max(0, i + win + ng), i + window + 1, n2 - 1)
            if left >= right:
                continue
            value = ''.join(c.unicode_repr for c in wrd.letters.ipa_chars[left:right])
            new_values.add(f'{prefix}{left}_{right}={value}')

    h.extend(list(new_values))


def write_ipa_stress_features(words_list, lang, name, max_grams=4, window=4, include_word_ngrams=True, overwrite=False):
    use2 = True
    feats_file = os.path.join(
        'feats',
        f'{name}_{lang}_ipa_stress_features_{max_grams}-grams_w{window}_'
        f'{"w" if include_word_ngrams else ""}_{"u2" if use2 else ""}.txt'
    )
    if os.path.isfile(feats_file) and overwrite is False:
        print(f"Warning: file {feats_file} already exists. Skipping...")
        return
    with open(feats_file, mode='w', encoding=ENCODING) as f:
        for word in words_list:
            tags = make_tags(word['ipa'], use2)
            j = 0
            for i, c in enumerate(word['ipa']):
                feats = feats_for_char(
                    word['ipa'], i, max_grams, lang, IPA_SYLLABLE_MARK, window, word_ngrams=include_word_ngrams)
                if len(feats) > 0:
                    f.write(f'{tags[j]}\t{feats}\n')
                    j += 1
            f.write('\n')


def write_data(lang, test_size=0.33):
    words = read_data(lang)
    words_train, words_test = train_test_split(words, test_size=test_size, random_state=42)
    write_ipa_stress_features(words_train, lang, f"train_{round(1-test_size, 2)}")
    write_ipa_stress_features(words_test, lang, f"test_{test_size}")


def write_all_data(lang, test_size):
    from preprocess import wiktionary as pw

    print(f"Preprocessing for {lang}")
    pw.write_stress_file(f"kaikki.org-dictionary-{lang}.json")
    print(f"Extracting features for {lang}")
    write_data(lang, test_size)


def write_all_data_for_all_languages(test_size):
    # languages = [
    # 'bg_BG', 'da_DK', 'de_DE', 'el_GR', 'es', 'it_IT', 'lt_LT', 'nb_NO', 'nn_NO', 'pt_PT', 'ro_RO', 'ru_RU',
    # 'sl_SL', 'sv_SE']
    languages = ['bg_BG', 'da_DK', 'de_DE', 'el_GR', 'es', 'it_IT', 'lt_LT', 'nb_NO', 'pt_PT', 'ro_RO', 'ru_RU']
    for language in languages:
        write_all_data(language, test_size)


def compute_baselines():
    languages = ['bg_BG', 'da_DK', 'de_DE', 'el_GR', 'es', 'it_IT', 'lt_LT', 'nb_NO', 'pt_PT', 'ro_RO']  # , 'ru_RU']
    all_counts = {}
    for language in languages:
        words = read_data(language)
        # words_train, words_test = train_test_split(words, test_size=0.2, random_state=42)
        ipa = [{'text': IPAString(unicode_string=word['ipa'])} for word in words]
        # ipa_test = [IPAString(unicode_string=word) for word in words_test]
        counts, cv_test = rcf.compute_baseline(ipa, language=language, cv_func1=make_ipa_cv, cv_func2=make_ipa_cv2)
        all_counts[language] = [counts, cv_test]
    for language1 in languages:
        _, cv_test = all_counts[language1]
        print(f"L1: {language1}")
        for language2 in languages:
            counts, _ = all_counts[language2]
            nr = 0
            nr2 = 0
            total = 0
            for _, cv1, cv2 in cv_test:
                total += 1
                if cv1 in counts:
                    if list(counts[cv1].keys())[0] == cv2:
                        nr += 1
                else:
                    nr2 += 1
            print(f"{language2}")
            print(len(counts))
            print(nr / total)
            print(nr, nr2, total)
            print((nr + nr2) / total)
        print("------")


if __name__ == "__main__":
    # write_data('bg_BG')
    # write_all_data_for_all_languages(0.33)
    write_all_data_for_all_languages(0.2)


# bg_BG
# 767
# 0.989993328885924
# 5936 60 5996
# 1.0
# da_DK
# 352
# 0.961768219832736
# 805 32 837
# 1.0
# de_DE
# 2339
# 0.9431355181576616
# 5324 321 5645
# 1.0
# el_GR
# 381
# 0.9441997063142438
# 643 38 681
# 1.0
# es
# 1619
# 0.9934221622533311
# 17670 117 17787
# 1.0
# it_IT
# 434
# 0.9833720030935809
# 2543 43 2586
# 1.0
# lt_LT
# 864
# 0.9587628865979382
# 1581 68 1649
# 1.0
# nb_NO
# 306
# 0.8687782805429864
# 192 29 221
# 1.0
# pt_PT
# 213
# 0.9859387923904053
# 1192 17 1209
# 1.0
# ro_RO
# 539
# 0.9689877121123464
# 1656 53 1709
# 1.0
# ru_RU
# 3565
# 0.9956813819577736
# 58100 252 58352
# 1.0


# In[2]: import wiktionary_crf_features as wcf
# 0.23.2
# 11.0.0
# In[3]: wcf.compute_baselines()
# bg_BG
# 767
# 0.989993328885924
# 5936 60 5996
# 1.0
# da_DK
# 352
# 0.961768219832736
# 805 32 837
# 1.0
# de_DE
# 2339
# 0.9431355181576616
# 5324 321 5645
# 1.0
# el_GR
# 381
# 0.9441997063142438
# 643 38 681
# 1.0
# es
# 1619
# 0.9934221622533311
# 17670 117 17787
# 1.0
# it_IT
# 434
# 0.9833720030935809
# 2543 43 2586
# 1.0
# lt_LT
# 864
# 0.9587628865979382
# 1581 68 1649
# 1.0
# nb_NO
# 306
# 0.8687782805429864
# 192 29 221
# 1.0
# pt_PT
# 213
# 0.9859387923904053
# 1192 17 1209
# 1.0
# ro_RO
# 539
# 0.9689877121123464
# 1656 53 1709
# 1.0
# L1: bg_BG
# bg_BG
# 767
# 0.989993328885924
# 5936 60 5996
# 1.0
# da_DK
# 352
# 0.72631754503002
# 4355 1641 5996
# 1.0
# de_DE
# 2339
# 0.8895930620413609
# 5334 662 5996
# 1.0
# el_GR
# 381
# 0.8684122748498999
# 5207 789 5996
# 1.0
# es
# 1619
# 0.9664776517678453
# 5795 201 5996
# 1.0
# it_IT
# 434
# 0.8654102735156771
# 5189 807 5996
# 1.0
# lt_LT
# 864
# 0.8865910607071381
# 5316 680 5996
# 1.0
# nb_NO
# 306
# 0.7036357571714477
# 4219 1777 5996
# 1.0
# pt_PT
# 213
# 0.8047031354236157
# 4825 1171 5996
# 1.0
# ro_RO
# 539
# 0.9112741827885257
# 5464 532 5996
# 1.0
# ------
# L1: da_DK
# bg_BG
# 767
# 0.7849462365591398
# 657 180 837
# 1.0
# da_DK
# 352
# 0.961768219832736
# 805 32 837
# 1.0
# de_DE
# 2339
# 0.966547192353644
# 809 28 837
# 1.0
# el_GR
# 381
# 0.8399044205495818
# 703 134 837
# 1.0
# es
# 1619
# 0.8661887694145759
# 725 112 837
# 1.0
# it_IT
# 434
# 0.7980884109916367
# 668 169 837
# 1.0
# lt_LT
# 864
# 0.7921146953405018
# 663 174 837
# 1.0
# nb_NO
# 306
# 0.7873357228195937
# 659 178 837
# 1.0
# pt_PT
# 213
# 0.8590203106332138
# 719 118 837
# 1.0
# ro_RO
# 539
# 0.9080047789725209
# 760 77 837
# 1.0
# ------
# L1: de_DE
# bg_BG
# 767
# 0.69140832595217
# 3903 1742 5645
# 1.0
# da_DK
# 352
# 0.7181576616474756
# 4054 1591 5645
# 1.0
# de_DE
# 2339
# 0.9431355181576616
# 5324 321 5645
# 1.0
# el_GR
# 381
# 0.5812223206377325
# 3281 2364 5645
# 1.0
# es
# 1619
# 0.7796279893711249
# 4401 1244 5645
# 1.0
# it_IT
# 434
# 0.5197519929140832
# 2934 2711 5645
# 1.0
# lt_LT
# 864
# 0.6692648361381753
# 3778 1867 5645
# 1.0
# nb_NO
# 306
# 0.5603188662533215
# 3163 2482 5645
# 1.0
# pt_PT
# 213
# 0.5679362267493357
# 3206 2439 5645
# 1.0
# ro_RO
# 539
# 0.7195748449955713
# 4062 1583 5645
# 1.0
# ------
# L1: el_GR
# bg_BG
# 767
# 0.8604992657856094
# 586 95 681
# 1.0
# da_DK
# 352
# 0.7929515418502202
# 540 141 681
# 1.0
# de_DE
# 2339
# 0.9236417033773862
# 629 52 681
# 1.0
# el_GR
# 381
# 0.9441997063142438
# 643 38 681
# 1.0
# es
# 1619
# 0.9353891336270191
# 637 44 681
# 1.0
# it_IT
# 434
# 0.7973568281938326
# 543 138 681
# 1.0
# lt_LT
# 864
# 0.8487518355359766
# 578 103 681
# 1.0
# nb_NO
# 306
# 0.7342143906020558
# 500 181 681
# 1.0
# pt_PT
# 213
# 0.8193832599118943
# 558 123 681
# 1.0
# ro_RO
# 539
# 0.8913362701908958
# 607 74 681
# 1.0
# ------
# L1: es
# bg_BG
# 767
# 0.9125203800528476
# 16231 1556 17787
# 1.0
# da_DK
# 352
# 0.6971945803114634
# 12401 5386 17787
# 1.0
# de_DE
# 2339
# 0.9177489177489178
# 16324 1463 17787
# 1.0
# el_GR
# 381
# 0.830887726991623
# 14779 3008 17787
# 1.0
# es
# 1619
# 0.9934221622533311
# 17670 117 17787
# 1.0
# it_IT
# 434
# 0.7183336144375105
# 12777 5010 17787
# 1.0
# lt_LT
# 864
# 0.8780007870916962
# 15617 2170 17787
# 1.0
# nb_NO
# 306
# 0.6867375049193231
# 12215 5572 17787
# 1.0
# pt_PT
# 213
# 0.7879350087142295
# 14015 3772 17787
# 1.0
# ro_RO
# 539
# 0.8870523415977961
# 15778 2009 17787
# 1.0
# ------
# L1: it_IT
# bg_BG
# 767
# 0.9559164733178654
# 2472 114 2586
# 1.0
# da_DK
# 352
# 0.8410672853828306
# 2175 411 2586
# 1.0
# de_DE
# 2339
# 0.9327146171693735
# 2412 174 2586
# 1.0
# el_GR
# 381
# 0.8936581593194122
# 2311 275 2586
# 1.0
# es
# 1619
# 0.9825986078886311
# 2541 45 2586
# 1.0
# it_IT
# 434
# 0.9833720030935809
# 2543 43 2586
# 1.0
# lt_LT
# 864
# 0.919953596287703
# 2379 207 2586
# 1.0
# nb_NO
# 306
# 0.8020108275328693
# 2074 512 2586
# 1.0
# pt_PT
# 213
# 0.871616395978345
# 2254 332 2586
# 1.0
# ro_RO
# 539
# 0.9280742459396751
# 2400 186 2586
# 1.0
# ------
# L1: lt_LT
# bg_BG
# 767
# 0.7252880533656761
# 1196 453 1649
# 1.0
# da_DK
# 352
# 0.5197089144936325
# 857 792 1649
# 1.0
# de_DE
# 2339
# 0.7926015767131595
# 1307 342 1649
# 1.0
# el_GR
# 381
# 0.6094602789569437
# 1005 644 1649
# 1.0
# es
# 1619
# 0.8186779866585809
# 1350 299 1649
# 1.0
# it_IT
# 434
# 0.5961188599151
# 983 666 1649
# 1.0
# lt_LT
# 864
# 0.9587628865979382
# 1581 68 1649
# 1.0
# nb_NO
# 306
# 0.4845360824742268
# 799 850 1649
# 1.0
# pt_PT
# 213
# 0.5154639175257731
# 850 799 1649
# 1.0
# ro_RO
# 539
# 0.6701030927835051
# 1105 544 1649
# 1.0
# ------
# L1: nb_NO
# bg_BG
# 767
# 0.7873303167420814
# 174 47 221
# 1.0
# da_DK
# 352
# 0.6063348416289592
# 134 87 221
# 1.0
# de_DE
# 2339
# 0.8099547511312217
# 179 42 221
# 1.0
# el_GR
# 381
# 0.5656108597285068
# 125 96 221
# 1.0
# es
# 1619
# 0.8144796380090498
# 180 41 221
# 1.0
# it_IT
# 434
# 0.5927601809954751
# 131 90 221
# 1.0
# lt_LT
# 864
# 0.7194570135746606
# 159 62 221
# 1.0
# nb_NO
# 306
# 0.8687782805429864
# 192 29 221
# 1.0
# pt_PT
# 213
# 0.5520361990950227
# 122 99 221
# 1.0
# ro_RO
# 539
# 0.7104072398190046
# 157 64 221
# 1.0
# ------
# L1: pt_PT
# bg_BG
# 767
# 0.9470636889991728
# 1145 64 1209
# 1.0
# da_DK
# 352
# 0.9594706368899917
# 1160 49 1209
# 1.0
# de_DE
# 2339
# 0.9867659222497932
# 1193 16 1209
# 1.0
# el_GR
# 381
# 0.9768403639371381
# 1181 28 1209
# 1.0
# es
# 1619
# 0.9851116625310173
# 1191 18 1209
# 1.0
# it_IT
# 434
# 0.9338296112489661
# 1129 80 1209
# 1.0
# lt_LT
# 864
# 0.9487179487179487
# 1147 62 1209
# 1.0
# nb_NO
# 306
# 0.9040529363110008
# 1093 116 1209
# 1.0
# pt_PT
# 213
# 0.9859387923904053
# 1192 17 1209
# 1.0
# ro_RO
# 539
# 0.9917287014061208
# 1199 10 1209
# 1.0
# ------
# L1: ro_RO
# bg_BG
# 767
# 0.9268578115857227
# 1584 125 1709
# 1.0
# da_DK
# 352
# 0.8812170860152135
# 1506 203 1709
# 1.0
# de_DE
# 2339
# 0.9619660620245758
# 1644 65 1709
# 1.0
# el_GR
# 381
# 0.8870684610883558
# 1516 193 1709
# 1.0
# es
# 1619
# 0.9526038619075483
# 1628 81 1709
# 1.0
# it_IT
# 434
# 0.802223522527794
# 1371 338 1709
# 1.0
# lt_LT
# 864
# 0.898186073727326
# 1535 174 1709
# 1.0
# nb_NO
# 306
# 0.7747220596840257
# 1324 385 1709
# 1.0
# pt_PT
# 213
# 0.8700994733762434
# 1487 222 1709
# 1.0
# ro_RO
# 539
# 0.9689877121123464
# 1656 53 1709
# 1.0
# ------
