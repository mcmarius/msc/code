TRAIN_SPLIT=0.67
TEST_SPLIT=0.33
#TRAIN_SPLIT=0.8
#TEST_SPLIT=0.2
NGRAMS=4
WINDOW=4
ITERATIONS=50
TYPE='w'  # word
TASK_TYPE='stress'
FEAT_TYPE='ipa'


wiktionary() {
for lang1 in bg_BG da_DK de_DE el_GR es it_IT lt_LT nb_NO nn_NO pt_PT ro_RO ru_RU sl_SL sv_SE
do
  echo "Training ${lang1}"
  FNAME="${FEAT_TYPE}_${lang1}_${TRAIN_SPLIT}_${TASK_TYPE}_${NGRAMS}-grams_w${WINDOW}_${TYPE}_i${ITERATIONS}"
  # OLDFNAME="${FEAT_TYPE}_${lang1}_${TASK_TYPE}_#${TRAIN_SPLIT}_m${NGRAMS}gw${WINDOW}_${TYPE}${ITERATIONS}"
  echo "crfsuite learn -pmax_iterations=${ITERATIONS} -aap -m models/${FNAME}.model -l -L logs/crf_train_${FNAME}.log.txt feats/train_${TRAIN_SPLIT}_${lang1}_${FEAT_TYPE}_${TASK_TYPE}_features_${NGRAMS}-grams_w${WINDOW}_${TYPE}_u2.txt > logs/train_${FNAME}.log.txt"
  crfsuite learn -pmax_iterations=${ITERATIONS} -aap -m models/${FNAME}.model -l -L logs/crf_train_${FNAME}.log.txt feats/train_${TRAIN_SPLIT}_${lang1}_${FEAT_TYPE}_${TASK_TYPE}_features_${NGRAMS}-grams_w${WINDOW}_${TYPE}_u2.txt > logs/train_${FNAME}.log.txt

  for lang2 in bg_BG da_DK de_DE el_GR es it_IT lt_LT nb_NO nn_NO pt_PT ro_RO ru_RU sl_SL sv_SE
  do
    echo "Evaluating ${lang1} on ${lang2}"
    TEST_FNAME="${FEAT_TYPE}_train_${lang1}_test_${lang2}_${TEST_SPLIT}_${TASK_TYPE}_${NGRAMS}-grams_w${WINDOW}_${TYPE}_i${ITERATIONS}"
    echo "crfsuite tag -m models/${FNAME}.model -t -q feats/test_${TEST_SPLIT}_${lang2}_${FEAT_TYPE}_${TASK_TYPE}_features_${NGRAMS}-grams_w${WINDOW}_${TYPE}_u2.txt > logs/test_${TEST_FNAME}.log.txt"
    crfsuite tag -m models/${FNAME}.model -t -q feats/test_${TEST_SPLIT}_${lang2}_${FEAT_TYPE}_${TASK_TYPE}_features_${NGRAMS}-grams_w${WINDOW}_${TYPE}_u2.txt > logs/test_${TEST_FNAME}.log.txt
  done
done
}

rsd_train() {
  lang1="rsd"
  FNAME="char_${lang1}_${TRAIN_SPLIT}_${TASK_TYPE}_${NGRAMS}-grams_w${WINDOW}_${TYPE}_i${ITERATIONS}"
  # OLDFNAME="${FEAT_TYPE}_${lang1}_${TASK_TYPE}_#${TRAIN_SPLIT}_m${NGRAMS}gw${WINDOW}_${TYPE}${ITERATIONS}"
  echo "crfsuite learn -pmax_iterations=${ITERATIONS} -aap -m models/${FNAME}.model -l -L logs/crf_train_${FNAME}.log.txt feats/train_${TRAIN_SPLIT}_${lang1}_char_${TASK_TYPE}_features_${NGRAMS}-grams_w${WINDOW}_${TYPE}_u2.txt > logs/train_${FNAME}.log.txt"
  crfsuite learn -pmax_iterations=${ITERATIONS} -aap -m models/${FNAME}.model -l -L logs/crf_train_${FNAME}.log.txt feats/train_${TRAIN_SPLIT}_${lang1}_char_${TASK_TYPE}_features_${NGRAMS}-grams_w${WINDOW}_${TYPE}_u2.txt > logs/train_${FNAME}.log.txt

}

rsd_eval() {
  lang1="rsd"
  FNAME="char_${lang1}_${TRAIN_SPLIT}_${TASK_TYPE}_${NGRAMS}-grams_w${WINDOW}_${TYPE}_i${ITERATIONS}"
  for lang2 in bg_BG da_DK de_DE el_GR es it_IT lt_LT nb_NO nn_NO pt_PT ro_RO ru_RU sl_SL sv_SE
  do
    echo "Evaluating ${lang1} on ${lang2}"
    TEST_FNAME="${FEAT_TYPE}_train_${lang1}_test_${lang2}_${TEST_SPLIT}_${TASK_TYPE}_${NGRAMS}-grams_w${WINDOW}_${TYPE}_i${ITERATIONS}"
    echo "crfsuite tag -m models/${FNAME}.model -t -q feats/test_${TEST_SPLIT}_${lang2}_${FEAT_TYPE}_${TASK_TYPE}_features_${NGRAMS}-grams_w${WINDOW}_${TYPE}_u2.txt > logs/test_${TEST_FNAME}.log.txt"
    crfsuite tag -m models/${FNAME}.model -t -q feats/test_${TEST_SPLIT}_${lang2}_${FEAT_TYPE}_${TASK_TYPE}_features_${NGRAMS}-grams_w${WINDOW}_${TYPE}_u2.txt > logs/test_${TEST_FNAME}.log.txt
  done
  lang2="rsd"
  FEAT_TYPE="char"
  echo "Evaluating ${lang1} on ${lang2}"
  TEST_FNAME="${FEAT_TYPE}_train_${lang1}_test_${lang2}_${TEST_SPLIT}_${TASK_TYPE}_${NGRAMS}-grams_w${WINDOW}_${TYPE}_i${ITERATIONS}"
  echo "crfsuite tag -m models/${FNAME}.model -t -q feats/test_${TEST_SPLIT}_${lang2}_${FEAT_TYPE}_${TASK_TYPE}_features_${NGRAMS}-grams_w${WINDOW}_${TYPE}_u2.txt > logs/test_${TEST_FNAME}.log.txt"
  crfsuite tag -m models/${FNAME}.model -t -q feats/test_${TEST_SPLIT}_${lang2}_${FEAT_TYPE}_${TASK_TYPE}_features_${NGRAMS}-grams_w${WINDOW}_${TYPE}_u2.txt > logs/test_${TEST_FNAME}.log.txt
}

wikt_eval_rsd() {
  FEAT_TYPE="ipa"
  lang2="rsd"
  for lang1 in bg_BG da_DK de_DE el_GR es it_IT lt_LT nb_NO nn_NO pt_PT ro_RO ru_RU sl_SL sv_SE
  do
    FNAME="${FEAT_TYPE}_${lang1}_${TRAIN_SPLIT}_${TASK_TYPE}_${NGRAMS}-grams_w${WINDOW}_${TYPE}_i${ITERATIONS}"
    echo "Evaluating ${lang1} on ${lang2}"
    TEST_FNAME="${FEAT_TYPE}_train_${lang1}_test_${lang2}_${TEST_SPLIT}_${TASK_TYPE}_${NGRAMS}-grams_w${WINDOW}_${TYPE}_i${ITERATIONS}"
    echo "crfsuite tag -m models/${FNAME}.model -t -q feats/test_${TEST_SPLIT}_${lang2}_char_${TASK_TYPE}_features_${NGRAMS}-grams_w${WINDOW}_${TYPE}_u2.txt > logs/test_${TEST_FNAME}.log.txt"
    crfsuite tag -m models/${FNAME}.model -t -q feats/test_${TEST_SPLIT}_${lang2}_char_${TASK_TYPE}_features_${NGRAMS}-grams_w${WINDOW}_${TYPE}_u2.txt > logs/test_${TEST_FNAME}.log.txt
  done
}

rsd() {
  # rsd_train
  rsd_eval
  wikt_eval_rsd
}

main() {
  # wiktionary
  rsd
  echo "Done."
}

main

